import React from 'react';
import {Link} from 'react-router';

export default React.createClass({
    updateCard() {
        this.props.onCartUpdate({
            id: this.props.card.get('id'),
            heading: this.refs.headerInput.value,
            description: this.refs.descriptionInput.value});
    },
    deleteCard() {
        this.props.onCartDelete({
            id: this.props.card.get('id'),
            sectionId: this.props.card.get('sectionId')
        });
    },

    // mixins: [History],
 
    // handleClose() {
    //     this.history.push('/');
    // },

    displayName: 'Card',

    render() {
        const heading = this.props.card.get('heading');
        const description = this.props.card.get('description');
        return (
            <div className="opened-card">
                <textarea className="heading" ref="headerInput" defaultValue={heading}/>
                <textarea className="description" ref="descriptionInput" defaultValue={description}/>
                <div className="actions">
                    <Link className="_link-without-decorations" onClick={this.updateCard} to={`/`}>
                        <div className="submit">save</div>
                    </Link>
                    <Link className="_link-without-decorations" to={`/`}>
                        <div className="close">cancel</div>
                    </Link>
                    <Link className="_link-without-decorations" to={`/`} onClick={this.deleteCard}>
                        <div className="delete" >delete</div>
                    </Link>
                </div>
            </div>
        )
    }
});
