import React from 'react';

export default React.createClass({
    propTypes: {
        onAddCard: React.PropTypes.func.isRequired
    },

    getInitialState() {
        return {isFormOpen: false};
    },

    openForm() {
        this.setState({isFormOpen: true});
    },

    closeForm() {
        this.setState({isFormOpen: false});
    },

    submitForm() {
        this.closeForm();
        this.props.onAddCard(this.refs.input.value);
    },

    render() {
        const {heading} = this.props;
        if (this.state.isFormOpen) {
            return (
                <div className="kanban-card-new-form">
                   <textarea rows="4" placeholder="Type..." ref='input' className="input" />
                   <div className="submit" onClick={this.submitForm}>Add</div>
                   <div className="cancel" onClick={this.closeForm}>Cancel</div>
                </div>
            );
        } else {
            return (
                <div className="add" onClick={this.openForm}>Add a card</div>
            );
        }
    }
});
