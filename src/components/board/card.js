import React from 'react';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import {Link} from 'react-router';

let Card = React.createClass({
    displayName: 'Card',

    propTypes: {
        card: React.PropTypes.object.isRequired
    },

    mixins: [PureRenderMixin],

    render() {
        const id = this.props.card.get('id');
        const heading = this.props.card.get('heading');


        return (
            <div className="kanban-card">
                <Link className="_link-without-decorations" to={`/${id}`}>
                    <div className="heading">{heading}</div>
                </Link>
            </div>
        );
    }
});


export default Card;