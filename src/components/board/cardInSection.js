import React from 'react';
import {Link} from 'react-router';
import {DragSource, DropTarget} from 'react-dnd';
import classNames from 'classnames';
import PureRenderMixin from 'react-addons-pure-render-mixin';
import Card from './card'
import * as ItemTypes from './item-types';

const source = {
    beginDrag(props) {
        return {
            id: props.card.get('id'),
            sectionId: props.card.get('sectionId')
        };
    },

    isDragging(props, monitor) {
        return props.card.get('id') === monitor.getItem().id
    }
};

function collectSource(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }
}

const target = {
    hover(props, monitor, component) {
        const dragCard = monitor.getItem();
        const dragId = dragCard.id;
        const dragSectionId = dragCard.sectionId;
        const hoverCard = props.card;
        const hoverId = hoverCard.get('id');

        if (dragId === hoverId) { return }
        props.onChange({id: dragId, 
            sectionId: hoverCard.get('sectionId'), 
            oldSectionId: dragSectionId,
            index: hoverCard.get('index')
        });
    }
};

function collectTarget(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget()
    };
}

let CardInSection = React.createClass({
    displayName: 'CardInSection',

    propTypes: {
        card: React.PropTypes.object.isRequired,
        onChange: React.PropTypes.func.isRequired,
        isDragging: React.PropTypes.bool.isRequired,
        connectDragSource: React.PropTypes.func.isRequired,
        connectDropTarget: React.PropTypes.func.isRequired
    },

    mixins: [PureRenderMixin],

    render() {
        const {connectDragSource, connectDropTarget, isDragging} = this.props;

        const classes = classNames({
            'kanban-card_in_section': true,
            '-placeholder': isDragging
        });

        return connectDropTarget(connectDragSource(
            <li className={classes}>
                <Card card={this.props.card.get('card')}/>
            </li>
        ));
    }
});
CardInSection = DragSource(ItemTypes.CARD, source, collectSource)(CardInSection);
CardInSection = DropTarget(ItemTypes.CARD, target, collectTarget)(CardInSection);

export default CardInSection;
