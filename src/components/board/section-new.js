import React from 'react';

export default React.createClass({
    propTypes: {
        onChange: React.PropTypes.func.isRequired
    },

    getInitialState() {
        return {isFormOpen: false};
    },

    openForm() {
        this.setState({isFormOpen: true});
    },

    closeForm() {
        this.setState({isFormOpen: false});
    },

    submitForm() {
        this.closeForm();
        console.log({heading: this.refs.input.value});
        this.props.onChange({heading: this.refs.input.value});
    },

    render() {
        const {heading} = this.props;
        if (this.state.isFormOpen) {
            return (
                <li className= "kanban-section-new-form">
                   <textarea rows="4" placeholder="Type..." ref='input' className="input" />
                   <div className="submit"  onClick={this.submitForm}>Add</div>
                   <div className="cancel"  onClick={this.closeForm}>Cancel</div>
                </li>
            );
        } else {
            return (
                <li className="add"  onClick={this.openForm}>Add a list...</li>
            );
        }
    }
});
