import React from 'react';
import Card from 'components/card';

import {connect} from 'react-redux'

import {getCard} from 'queries';
import {updateCard, deleteCard} from 'actions';

const Container = React.createClass({
    propTypes: {
        card: React.PropTypes.object.isRequired
    },

    render() {
        const {dispatch, card} = this.props;
        return (
            <Card card={card}
                  onCartUpdate={attrs => dispatch(updateCard(attrs))}
                  onCartDelete={attrs => dispatch(deleteCard(attrs))}/>
        )
    }
});

function mapStateToProps(state, ownProps) {
    const cardId = +ownProps.params.id;
    return {card: getCard(state, cardId)};
}

export default connect(mapStateToProps)(Container);
