import React from 'react';
import {Route, IndexRoute} from 'react-router';

import App from 'containers/app';
import CardContainer from 'containers/card-container';

const routes =
    <Route path="/" component={App} >
        <Route path=":id" component={CardContainer} />
    </Route>;

export default routes;
