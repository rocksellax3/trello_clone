import {fromJS} from 'immutable';

export function sectionsIds(state) {
    return state.getIn(['sections']);
}

export function getCard(state, id) {
    let sectionId = state.getIn(['sectionByCard', id]);
    return state.getIn(['entities', 'cards', id]).set('sectionId', sectionId);
}

export function sections(state) {
    let a = state
        .getIn(['sections'])
        .map(sectionId => {
            let section = state.getIn(['entities', 'sections', sectionId]);
            let cardsInSection = section.get('cards')
                .map((cardId, index) => 
                    fromJS({
                        card: state.getIn(['entities', 'cards', cardId]),
                        id: cardId,
                        index: index,
                        sectionId: sectionId
                    })
                );
            return section.delete('cards')
                    .set('cardsInSection', cardsInSection)
        });

        // console.log(a.toJS());
        return a;
}
